'use strict';

bookingApp.controller('BookingListController',
    function BookingListController($scope, bookingData) {
        $scope.bookings = bookingData.getAllBookings();
    }
);

