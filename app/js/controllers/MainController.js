'use strict';

bookingApp.controller('MainController', function ($scope, $location) {
    $scope.newBooking = function(){
        $location.replace();
        $location.url('booking');
    };
    $scope.seatBooking = function(){
        $location.replace();
        $location.url('seating');
    };
  });
