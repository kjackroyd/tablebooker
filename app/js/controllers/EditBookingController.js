'use strict';

bookingApp.controller('EditBookingController',
    function EditBookingController($scope, bookingData, $location, $routeParams, $timeout) {

        $scope.booking = {};
        $scope.showDatePicker = false;
        $scope.editingBooking = $location.$$url.indexOf('/booking/edit') > -1;

        if ($scope.editingBooking) {
            $scope.booking = bookingData.getBooking($routeParams.bookingId);
        }

        $scope.saveBooking = function (booking, form) {
            if (form.$invalid) return;

            bookingData.save(booking);
            $location.url('/booking/' + booking.id);
        };

        $scope.cancelEdit = function () {
            $location.url("/bookings");
        };

        $scope.dateFocus = function() {
            $scope.showDatePicker = true;
        };

        $scope.dateBlur = function() {
            $timeout(function() {$scope.showDatePicker = false; }, 200);
        };

        $scope.setDateFromPicker = function(date) {
            $scope.booking.date = date;
            $scope.showDatePicker = false;
        };
    }
);