'use strict';

bookingApp.controller('BookingController',
    function BookingController($scope, $routeParams, $location, bookingData) {
        $scope.booking = bookingData.getBooking($routeParams.bookingId);

        $scope.editBooking = function (booking) {
            $location.url('/booking/edit/' + booking.id);
        };

        $scope.seat = function (booking) {
            booking.seated = true;
            bookingData.save(booking);
            $location.url('/bookings');
        };

        $scope.unseat = function (booking) {
            booking.seated = false;
            bookingData.save(booking);
            $location.url('/bookings');
        };

        $scope.delete = function(bookingId){
            bookingData.delete(bookingId);
            $location.url('/bookings');
        };
    }
);