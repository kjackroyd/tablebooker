'use strict';

bookingApp.directive('navigation', function () {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/templates/directives/navigation.html'
    };
  });
