'use strict';

bookingApp.directive('bookingSummary', function() {
    return {
        restrict: 'E',
        templateUrl: '/templates/directives/bookingSummary.html',
        replace: true,
        scope: {
            booking: "="
        }
    }
});
