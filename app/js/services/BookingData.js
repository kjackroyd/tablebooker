'use strict';

bookingApp.factory('bookingData', function ($rootScope, localStorage) {

    var localStorageId = 'bookings';
    var bookingsJson = localStorage[localStorageId];

    var bookings = bookingsJson ? JSON.parse(bookingsJson) : [];

    $rootScope.$watch(function() { return bookings; }, function() {
        localStorage[localStorageId] = JSON.stringify(bookings);
    }, true);

    return {
        getBooking: function(bookingId) {
            var booking = null;
            var results = $.grep(bookings, function(obj){ return obj.id && obj.id == bookingId; });

            if(results.length > 0){
                booking = results[0];
            }

            return booking;
        },
        getAllBookings: function() {
            return bookings;
        },
        save: function(booking) {
            if (booking.id) {
                $.each(bookings, function(index, value) {
                    if(value.id === booking.id){
                        value.firstName = booking.firstName;
                        value.lastName = booking.lastName;
                        value.date = booking.date;
                        value.time = booking.time;
                        value.covers = booking.covers;
                        value.phoneNo = booking.phoneNo;
                        value.email = booking.email;
                        value.seated = booking.seated;
                    }
                });
            } else {
                booking.id = getNextBookingId(bookings);
                booking.seated = false;
                bookings.push(booking);
            }
        },
        delete: function(bookingId) {
            $.each(bookings, function(index, value) {
                if(value.id === bookingId){
                    bookings.splice(index, 1);
                }
            });
        }
    };

    function getNextBookingId(bookings) {
        var max = 0;
        for (var i = 0; i < bookings.length; i++) {
            if (bookings[i].id > max) {
                max = bookings[i].id;
            }
        }
        return max+1;
    }
});