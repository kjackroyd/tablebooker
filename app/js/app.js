'use strict';

var bookingApp = angular.module('bookingApp', ['ngResource']);

bookingApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.when('/bookings', {templateUrl: '/templates/bookingList.html', controller: 'BookingListController'});
    $routeProvider.when('/booking/new', {templateUrl: '/templates/editBooking.html', controller: 'EditBookingController'});
    $routeProvider.when('/booking/edit/:bookingId', {templateUrl: '/templates/editBooking.html', controller: 'EditBookingController'});
    $routeProvider.when('/booking/:bookingId', {templateUrl: '/templates/BookingDetails.html', controller: 'BookingController'});
    $routeProvider.otherwise({redirectTo: '/bookings'});
    $locationProvider.html5Mode(true);
}]);